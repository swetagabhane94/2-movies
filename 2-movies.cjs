const favouritesMovies = {
  Matrix: {
    imdbRating: 8.3,
    actors: ["Keanu Reeves", "Carrie-Anniee"],
    oscarNominations: 2,
    genre: ["sci-fi", "adventure"],
    totalEarnings: "$680M",
  },
  FightClub: {
    imdbRating: 8.8,
    actors: ["Edward Norton", "Brad Pitt"],
    oscarNominations: 6,
    genre: ["thriller", "drama"],
    totalEarnings: "$350M",
  },
  Inception: {
    imdbRating: 8.3,
    actors: ["Tom Hardy", "Leonardo Dicaprio"],
    oscarNominations: 12,
    genre: ["sci-fi", "adventure"],
    totalEarnings: "$870M",
  },
  "The Dark Knight": {
    imdbRating: 8.9,
    actors: ["Christian Bale", "Heath Ledger"],
    oscarNominations: 12,
    genre: ["thriller"],
    totalEarnings: "$744M",
  },
  "Pulp Fiction": {
    imdbRating: 8.3,
    actors: ["Sameul L. Jackson", "Bruce Willis"],
    oscarNominations: 7,
    genre: ["drama", "crime"],
    totalEarnings: "$455M",
  },
  Titanic: {
    imdbRating: 8.3,
    actors: ["Leonardo Dicaprio", "Kate Winslet"],
    oscarNominations: 13,
    genre: ["drama"],
    totalEarnings: "$800M",
  },
};

// NOTE: For all questions, the returned data must contain all the movie information including its name.

// Q1. Find all the movies with total earnings more than $500M.

const moviesEarning = Object.entries(favouritesMovies).reduce((data, movie) => {
  if (movie[1].totalEarnings > '$500') {
    data[movie[0]] = movie[1];
  }

  return data;
}, {});

// console.log(moviesEarning);

// Q2. Find all the movies who g\ot more than 3 oscarNominations and also totalEarning are more than $500M.

const moviesOscarAndEarning = Object.entries(moviesEarning).reduce(
  (data, movie) => {
    if (Number(movie[1].oscarNominations) > 3) {
      data[movie[0]] = movie[1];
    }

    return data;
  },
  {}
);
//console.log(moviesOscarAndEarning);

// Q.3 Find all movies of the actor "Leonardo Dicaprio".
const movieActor = Object.entries(favouritesMovies).reduce((data, movie) => {
  if (movie[1].actors.includes("Leonardo Dicaprio")) {
    data[movie[0]] = movie[1];
  }
  return data;
}, {});

//console.log(movieActor)

// Q.4 Sort movies (based on IMDB rating)
//     if IMDB ratings are same, compare totalEarning as the secondary metric.
const movieSorting = Object.fromEntries(
  Object.entries(favouritesMovies).sort((a, b) => {
    if (a[1].imdbRating > b[1].imdbRating) {
      return 1;
    } else if (a[1].imdbRating < b[1].imdbRating) {
      return -1;
    } else {
      if (
        Number(a[1].totalEarnings.slice(1, -1)) >
        Number(b[1].totalEarnings.slice(1, -1))
      ) {
        return 1;
      } else if (
        Number(a[1].totalEarnings.slice(1, -1)) <
        Number(b[1].totalEarnings.slice(1, -1))
      ) {
        return -1;
      } else {
        return 0;
      }
    }
  })
);

//console.log(movieSorting);

// Q.5 Group movies based on genre. Priority of genres in case of multiple genres present are:
//     drama > sci-fi > adventure > thriller > crime

// NOTE: Do not change the name of this file
